from django.conf import settings
from django.contrib import messages
from django.utils.safestring import mark_safe
from django.utils.html import escape
from django.urls import reverse

from crum import get_current_request, get_current_user

from sass_processor.processor import sass_processor

from py2js.transpiler import transpile_body
from ryzom_django_mdc.html import *

from djlang.utils import gettext as _

from electeez_common.components import Messages
from electeez_common.components import Body as CommonBody


class Body(Body):
    def __init__(self, main_component, **context):
        self.main_component = main_component
        super().__init__(**context)

    def to_html(self, **context):
        main_html = self.main_component.to_html(**context)
        self.content += [
            Header(),
            main_html,
            Footer()
        ]
        return super().to_html(**context)


class Header(Div):
    def __init__(self):
        user = get_current_user()
        welcome = _(
            'Welcome to %(app)s',
            app=Span(_('NeuillyVote'), cls='red').to_html()
        )
        super().__init__(
            Div(
                Span(
                    A(
                        href=reverse('home'),
                        cls='top-panel-logo'
                    ),
                    cls='link',
                    style=dict(
                        width='180px',
                        height='100px',
                        margin='25px 50px',
                    ),
                ),
                Span(
                    MDCButtonRaised(
                        _('log in'),
                        tag='a',
                        href=reverse('login'),
                        style=dict(
                            background_color='var(--danger)'
                        )
                    ) if not user or not user.pk else
                    MDCButtonRaised(
                        _('log out'),
                        tag='a',
                        href=reverse('logout'),
                        style=dict(
                            background_color='var(--danger)'
                        )
                    ),
                    style=dict(
                        margin='25px 50px',
                    ),
                ),
                Span(
                    B(
                        I(
                            'how_to_vote',
                            cls='material-icons',
                            style=dict(
                                font_size='72px',
                                display='inline-block',
                                position='relative',
                                bottom='-6px'
                            )
                        ),
                        ' ',
                        welcome
                    ),
                    style=dict(
                        width='100%',
                        text_align='center',
                        font_size='2.5em',
                        font_family="'Open Sans', Arial, Helvetica, sans-serif"
                    )
                ),
                cls='top-panel',
                style=dict(
                    width='100%',
                    padding_bottom='100px',
                    flex_direction='row'
                )
            )
        )


class Footer(Footer):
    sass = '''
    footer.Footer
        font-size: 19px
        a, a:visited
            color: black
            text-decoration: underline
    '''
    def __init__(self):
        super().__init__(
            Div(
                _('Made by %(electis)s', electis=A(_('Electis.io'), href='https://electis.io').to_html()),
                style=dict(
                    width='100%',
                    text_align='center',
                    margin='32px'
                )
            ),
            Hr(style=dict(width='150px', margin='24px')),
            Div(
                A(_('Legal notices'), href=reverse('legal'), style=dict(margin='12px')),
                A(_('Data privacy policy'), href=reverse('policy'), style=dict(margin='12px')),
                A(_('FAQ'), href=reverse('faq'), style=dict(margin='12px')),
                style=dict(
                    display='flex',
                    flex_flow='row wrap',
                    justify_content='center'
                )
            ),
            Hr(style=dict(width='150px', margin='24px')),
            Div(
                A(
                    Img(
                        alt='logo neuilly',
                        src='/static/logo.png',
                        width='340px',
                        style=dict(
                            filter='brightness(0)'
                        )
                    ),
                    href='https://www.neuillysurseine.fr'
                ),
                style=dict(
                    margin="64px auto",
                    width='100%',
                ),
            ),
            style=dict(
                display='flex',
                flex_flow='row wrap',
                justify_content='center',
                align_items='center',
            ),
            cls='footer'
        )

CommonBody.footer_class = Footer


class LandingApp(Html):
    body_class = Body

    def to_html(self, head, body, **ctx):
        if settings.DEBUG:
            common_style_src = sass_processor('css/common.scss')
            style_src = sass_processor('css/style.scss')
        else:
            common_style_src = '/static/css/common.css'
            style_src = '/static/css/style.css'

        head.addchild(Stylesheet(href=common_style_src))
        head.addchild(Stylesheet(href=style_src))
        head.addchild(Stylesheet(href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;700&display=swap"))
        head.addchild(
            Script(
                src='https://privacyportalde-cdn.onetrust.com/consent-receipt-scripts/scripts/otconsent-1.0.min.js',
                type="text/javascript",
                data_ot_id="40ec4ddd-1627-4efb-b428-a53e211552ad",
                charset="UTF-8",
                id="consent-receipt-script",
                data_ot_consent_register_event="document-load"
            )
        )

        return super().to_html(head, body, **ctx)


@template('landing', LandingApp)
class LandingPage(Main):
    def to_html(self, *content, view, **context):
        msgs = messages.get_messages(view.request)
        return super().to_html(
            Div(
                mark_safe(_('LANDING_INFO_PANEL_MSG').replace('\n', '<br>')),
                cls='info-panel',
            ),
            Div(
                H3(
                    _('Take part to an upcoming vote'),
                    style=dict(
                        font_size='24px',
                        font_weight=400,
                        text_align='center',
                        margin_top='24px',
                        margin_bottom='12px',
                    )
                ),
                Div(
                    *(msg.message for msg in msgs),
                    style=dict(
                        margin='32px auto',
                        padding='32px',
                        background='aliceblue',
                        color='yellowgreen',
                        text_align='center',
                        max_width='600px'
                    )
                ) if msgs else
                Form(
                    MDCTextFieldOutlined(
                        Input(type='email', id='inputEmail', name='inputEmail', required=True),
                        label=_('Email address'),
                        help_text=_('Type your email to register to this service'),
                        addcls='ot-form-control',
                        style=dict(
                            max_width='520px',
                            margin='0px auto',
                            margin_bottom='0px',
                        )
                    ),
                    MDCCheckboxField(
                        MDCCheckboxInput(
                            value='Neuilly Vote',
                            required=True,
                        ),
                        name='confirmation',
                        label=Span(
                            _('I consent to the use of my email address'),
                            ' ',
                            A(I('help_outline', cls='material-icons'), cls='learn-more', style=dict(font_size='0.8em')),
                            style=dict(
                                font_size='18px',
                                font_weight=400,
                                font_style='italic',
                                text_align='center',
                                margin='12px',
                            )
                        ),
                    ),
                    Div(
                        Div(Span(_('SUBSCRIBE_LEARN_MORE_TEXT')), cls='learn-more-inner'),
                        style=dict(
                            width='600px',
                            margin='24px auto',
                            overflow='hidden',
                            transition='max-height 0.1s ease-out 0.1s',
                            color='royalblue'
                        ),
                        cls='learn-more-text'
                    ),
                    MDCButtonOutlined(
                        _('I take part'),
                        id='trigger',
                        addcls='ot-submit-button',
                        style=dict(
                            margin='50px auto',
                            font_size='32px',
                            color='black',
                            padding='32px 64px',
                            border_radius='8px',
                            font_weight=400,
                            border='4px solid black',
                        ),
                    ),
                    action='',
                    method='POST',
                    data_ot_cp_id="cd2cd391-6c19-4ec3-bdd8-b0d024bca161-active",
                    cls='agree-form',
                    addcls='ot-form-consent',
                    id='ot-form-consent',
                ),
            ),
        )

    def py2js(self):
        def openDialog():
            elem = document.querySelector('.learn-more-text')
            current = elem.style.maxHeight
            if current == '0px':
                elem.style.maxHeight = elem.initial_height
            else:
                elem.style.maxHeight = 0

        elem = document.querySelector('.learn-more-text')
        if elem:
            elem.initial_height = elem.clientHeight + 'px'
            elem.style.maxHeight = 0
            document.querySelector('.learn-more').onclick = openDialog


@template('legal', LandingApp)
class Legal(Div):
    def to_html(self, *content, **context):
        return super().to_html(
            Div(
                H4(
                    _('Legal notices'),
                    style=dict(
                        text_align='center'
                    )
                ),
                H5(_('Edition')),
                Div(
                    mark_safe(_('LEGAL_EDITION_TEXT').replace('\n', '<br>'))
                ),
                H5(_('Author rights')),
                Div(
                   mark_safe(_('AUTHOR_RIGHTS_TEXT').replace('\n', '<br>'))
                ),
                cls='info-panel',
            )
        )


@template('policy', LandingApp)
class Legal(Div):
    def to_html(self, *content, **context):
        return super().to_html(
            Div(
                H4(
                    _('Data privacy policy'),
                    style=dict(
                        text_align='center'
                    )
                ),
                Div(
                    mark_safe('''<!-- OneTrust Privacy Notice start -->
  <!-- Container in which the privacy notice will be rendered -->
  <div id="otnotice-e2599a30-e42d-4991-8e52-c386265763b7" class="otnotice"></div>

  <script src="https://privacyportalde-cdn.onetrust.com/privacy-notice-scripts/otnotice-1.0.min.js" type="text/javascript" charset="UTF-8" id="otprivacy-notice-script">
      settings="eyJjYWxsYmFja1VybCI6Imh0dHBzOi8vcHJpdmFjeXBvcnRhbC1kZS5vbmV0cnVzdC5jb20vcmVxdWVzdC92MS9wcml2YWN5Tm90aWNlcy9zdGF0cy92aWV3cyJ9"
    </script>

  <script type="text/javascript" charset="UTF-8">
      // To ensure external settings are loaded, use the Initialized promise:
      OneTrust.NoticeApi.Initialized.then(function() {
        OneTrust.NoticeApi.LoadNotices(["https://privacyportalde-cdn.onetrust.com/40ec4ddd-1627-4efb-b428-a53e211552ad/privacy-notices/e2599a30-e42d-4991-8e52-c386265763b7.json"]);
      });
    </script>

  <!-- OneTrust Privacy Notice end -->'''),
                ),
                cls='info-panel',
            )
        )


@template('cookies', LandingApp)
class Legal(Div):
    def to_html(self, *content, **context):
        return super().to_html(
            Div(
                H4(
                    _('Cookies'),
                    style=dict(
                        text_align='center'
                    )
                ),
                Div(mark_safe(_('COOKIES_TEXT').replace('\n', '<br>'))),
                cls='info-panel',
            )
        )


@template('faq', LandingApp)
class Legal(Div):
    def to_html(self, *content, **context):
        return super().to_html(
            Div(
                H4(
                    _('FAQ'),
                    style=dict(
                        text_align='center'
                    )
                ),
                Div(
                    mark_safe(
                        _('FAQ_TEXT').replace(
                            '\n', '<br>'
                        ).replace(
                            'TITLE_START', '<h5>'
                        ).replace(
                            'TITLE_END', '</h5>'
                        )
                    )
                ),
                cls='info-panel faq',
            )
        )


@template('unsubscribe', LandingApp)
class Legal(Div):
    def to_html(self, *content, view, **context):
        msgs = messages.get_messages(view.request)
        return super().to_html(
            Div(
                H4(
                    _('Unsubscribe from voting list'),
                    style=dict(
                        text_align='center'
                    )
                ),
                Div(
                    Div(
                        *(msg.message for msg in msgs),
                        style=dict(
                            margin='32px auto',
                            padding='32px',
                            background='aliceblue',
                            color='yellowgreen',
                            text_align='center',
                            max_width='600px'
                        )
                    ) if msgs else
                    Form(
                        MDCButtonOutlined(
                            _('I want to unsubscribe'),
                            style=dict(
                                margin='50px auto',
                                font_size='32px',
                                color='black',
                                padding='32px 64px',
                                border_radius='8px',
                                font_weight=400,
                                border='4px solid black',
                            ),
                        ),
                        CSRFInput(context['request']),
                        method='POST',
                    ),
                    style=dict(
                        display='flex',
                        justify_content='center'
                    )
                ),
                cls='info-panel faq',
            )
        )
