import requests

from django import forms
from django.contrib.auth.decorators import login_required

from electeez_common.urls import *

from djlang.utils import gettext as _


class LegalView(generic.TemplateView):
    template_name = 'legal'


class DataPolicyView(generic.TemplateView):
    template_name = 'policy'


class CookieView(generic.TemplateView):
    template_name = 'cookies'


class FAQView(generic.TemplateView):
    template_name = 'faq'


class UnsubscribeView(generic.FormView):
    template_name = 'unsubscribe'

    class form_class(forms.Form):
        pass

    def form_valid(self, form):
        res = requests.post(
            settings.ONE_TRUST_CONSENT_API_URL,
            json=dict(
                identifier=self.request.user.email,
                requestInformation=settings.ONE_TRUST_CONSENT_API_KEY,
                purposes=[
                    dict(
                        Id=settings.ONE_TRUST_CONSENT_PURPOSE,
                        TransactionType='WITHDRAWN'
                    )
                ]
            )
        )
        return super().form_valid(form)

    def get_success_url(self):
        messages.success(
            self.request,
            _('You successfully unsubscribed from NeuillyVote mailing list'))
        return reverse('unsubscribe')


urlpatterns += i18n_patterns(
    path('legal/', LegalView.as_view(), name='legal'),
    path('policy/', DataPolicyView.as_view(), name='policy'),
    path('cookies/', CookieView.as_view(), name='cookies'),
    path('faq/', FAQView.as_view(), name='faq'),
    path('unsubscribe/', login_required(UnsubscribeView.as_view()), name='unsubscribe'),
)
