from django.core.cache import cache
from django.db import models
from django.conf import settings
from django.contrib.sites.models import Site, SiteManager
from django.core.files.storage import FileSystemStorage

from electeez_consent.models import Provider


class SiteManager(SiteManager):
    def get_current(self):
        site = None
        cached = cache.get('current_site')
        if cached:
            return cached
        site = Site.objects.get(id=settings.SITE_ID)
        cache.set('current_site', site)
        return site



class Site(Site):
    contact_email = models.EmailField(default='contact@electis.app')
    sender_email = models.EmailField(default='contact@electis.app')

    all_users_can_create = models.BooleanField(default=True)
    all_results_are_visible = models.BooleanField(default=True)
    registration_allowed = models.BooleanField(default=True)

    footer_url = models.CharField(max_length=255, default='https://electis.app')

    email_banner_fg = models.CharField(
        max_length=7,
        default='#000000'
    )

    email_banner_bg = models.CharField(
        max_length=7,
        default='#ffffff'
    )

    email_banner = models.ImageField(
        upload_to=f'{settings.STATIC_ROOT_DIR}/images',
        null=True,
        blank=True
    )

    email_banner_url = models.URLField(
        null=True, blank=True
    )

    email_footer_fg = models.CharField(
        max_length=7,
        default='#000000'
    )

    email_footer_bg = models.CharField(
        max_length=7,
        default='#ffffff'
    )

    email_footer = models.ImageField(
        upload_to=f'{settings.STATIC_ROOT_DIR}/images',
        null=True,
        blank=True
    )

    email_footer_url = models.URLField(
        null=True, blank=True
    )

    consent_provider = models.ForeignKey(
        Provider,
        on_delete=models.SET_NULL,
        blank=True, null=True
    )

    objects = SiteManager()

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        cache.set('current_site', self)
