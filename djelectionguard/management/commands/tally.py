import importlib
import inspect
import json
import re

from django.db import (
    DEFAULT_DB_ALIAS, connections, transaction
)
from django.apps import apps
from django.conf import settings
from django.core.management.base import BaseCommand, CommandError
from django.utils import translation

from pymemcache.client.base import Client

from djelectionguard.models import Contest, decrypt_contest


class Command(BaseCommand):
    help = 'Tally & decrypt an election'

    def handle(self, *args, **options):
        to_decrypt = Contest.objects.filter(decrypting=True)
        client = Client(settings.MEMCACHED_HOST)
        for contest in to_decrypt:
            print(f'Decryption of "{contest.name}" started')
            params_str = client.get(f'{contest.pk}-decrypt_params')
            if not params_str:
                print('Decryption params were not found in memory')
                print('Erasing keys and set contest to the not decrypted state')
                contest.decrypting = False
                for guardian in contest.guardian_set.all():
                    guardian.uploaded = None
                    guardian.save()
                contest.save()
                continue
            params = json.loads(params_str)
            decrypt_contest(*params.values())
            print(f'Decryption of "{contest.name}" finished')

