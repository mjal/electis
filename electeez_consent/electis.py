import json

from djlang.utils import gettext as _

from .base import ProviderBase


class Provider(ProviderBase):
    def update_consent(self, user, value):
        user.consent.get_or_create(value=value)
        user.consent.update(value=value)

    def get_consent_users(self, date):
        return Consent.objects.filter(
            value='CONFIRMED'
        ).values_list(
            'user__email',
            flat=True
        )
