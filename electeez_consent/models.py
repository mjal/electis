import importlib
import json

from datetime import datetime

from django.db import models
from django.conf import settings
from django.core.exceptions import ValidationError
from django.core.validators import validate_email


SETTINGS = dict(
    PROVIDERS = (
        ('electeez_consent.manual.Provider', 'Manual'),
        ('electeez_consent.onetrust.Provider', 'OneTrust'),
        ('electeez_consent.electis.Provider', 'Electis'),
    )
)


class Provider(models.Model):
    name = models.CharField(
        max_length=255
    )
    provider_class = models.CharField(
        max_length=255,
        choices=SETTINGS['PROVIDERS']
    )
    props = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.name

    @property
    def provider(self):
        _mod, cls = self.provider_class.rsplit('.', 1)
        provider_class = getattr(importlib.import_module(_mod), cls)
        return provider_class(self)

    @property
    def parameters(self):
        props = self.props or '{}'
        return json.loads(props) or dict()

    @parameters.setter
    def parameters(self, value):
        self.props = json.dumps(value)
        self.save()

    def get_active_emails(self):
        provider = self.provider
        valid, result = provider.get_updates(None)

        if valid:
            filtered_emails = []
            for raw_email in result:
                email = raw_email.lower()
                try:
                    validate_email(email)
                except ValidationError as e:
                    #provider.remove_consent(e)
                    print(f'Removing consent for invalid email {email}')
                    continue
                filtered_emails.append(email)
            return filtered_emails


class ProviderAccess(models.Model):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE
    )
    provider = models.ForeignKey(
        Provider,
        on_delete=models.CASCADE
    )


class Consent(models.Model):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE
    )
    provider = models.ForeignKey(
        ProviderAccess,
        on_delete=models.CASCADE
    )
    value = models.CharField(
        max_length=255,
    )
