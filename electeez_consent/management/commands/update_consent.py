import importlib
import inspect
import json
import re

from django.db import (
    DEFAULT_DB_ALIAS, connections, transaction
)
from django.apps import apps
from django.conf import settings
from django.core.management.base import BaseCommand, CommandError
from django.utils import translation
from django.urls import reverse

from djelectionguard.models import Contest


class Command(BaseCommand):
    help = 'Update voters of running elections'

    def handle(self, *args, **options):
        contests = Contest.objects.filter(
            actual_end=None,
        ).exclude(
            actual_start=None
        )

        for contest in contests:
            contest.update_voters()
            if contest.voter_set.exclude(open_email_sent=None).count():
                contest.send_mail(
                    contest.open_email_title,
                    contest.open_email,
                    reverse('contest_vote', args=[contest.pk]),
                    'open_email_sent'
                )
