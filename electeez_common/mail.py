import os

from pathlib import Path
from email.mime.image import MIMEImage
from django.core.mail import EmailMultiAlternatives
from django.conf import settings
from django.template.loader import render_to_string
from django.utils.safestring import mark_safe
from django.utils.html import escape
from djlang.utils import gettext as _

from electeez_sites.models import Site
from electeez_auth.models import User


def attach_image(email, image):
    subtype = None
    if os.path.splitext(image.name)[1] == '.svg':
        subtype = 'svg+xml'

    to_attach = MIMEImage(image.file.read(), _subtype=subtype)
    to_attach.add_header('Content-ID', f"<{image.name}>")
    email.attach(to_attach)


def send_mail(subject, body, sender, recipient):
    email = EmailMultiAlternatives(subject=subject, body=body, from_email=sender, to=recipient)

    site = Site.objects.get_current()
    if site.email_banner or site.email_footer:

        user = User.objects.filter(email__in=recipient).first()
        unsub_url = None
        if user:
            otp = user.otp_new(redirect='/unsubscribe/')
            unsub_url = otp.url

        email_html = render_to_string('email_template.html', context=dict(
            header_bg_color=site.email_banner_bg,
            header_fg_color=site.email_banner_fg,
            header_text=_('EMAIL_BANNER_TEXT'),
            header_image=site.email_banner.name if site.email_banner else None,
            header_url=site.email_banner_url,
            body_text=mark_safe(body.replace('\n', '<br>')),
            footer_bg_color=site.email_footer_bg,
            footer_fg_color=site.email_footer_fg,
            footer_text=mark_safe(_('EMAIL_FOOTER_TEXT').replace('\n', '<br>')),
            footer_image=site.email_footer.name if site.email_footer else None,
            footer_url=site.email_banner_url,
            unsub_url=unsub_url
        ))
        email = EmailMultiAlternatives(subject=subject, body=email_html, from_email=sender, to=recipient)
        email.attach_alternative(email_html, "text/html")
        email.content_subtype = 'html'  # set the primary content to be text/html
        email.mixed_subtype = 'related' # it is an important part that ensures embedding of an image
        if site.email_banner:
            attach_image(email, site.email_banner)
        if site.email_footer:
            attach_image(email, site.email_footer)

    email.send()
